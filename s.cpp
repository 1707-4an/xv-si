#include <iostream>
#include <fstream>
#include <time.h>
#include <stdlib.h>
void fillarr(double *arr, int n); //наполнение массива
void printarr(double *arr, int n); //dsdtcnb массив
double *createarr(int n); //создание массива
void reassign(double *&arr, int &n, int k, double number); //переназначить эл. массива
void del(double *&arr, int &n, int k); //удалить эл. массива
void n7(double *&arr, int &n, double number, double x);
void n8(double *&arr, int &n, double x);
int check_file(); //сканирование файла, определяем кл-во элементов массива
void number_n(double &number); //
using namespace std;
int main()
{ setlocale(LC_ALL,"russian");
  int n = check_file(); //сканируем файл
  if (!n)
  {
    cout << "данные файла неверны" << endl;
    return 0;
  } //проверка на наличие данных 
  double *arr = createarr(n); //создаем указатель на массив
  fillarr(arr, n); //наполняем массив
  double number; 
  int m = 1;
  while (m) //номер кейса/задания
  {
    system("cls"); //очистка экрана консоли
    cout << "Массив: ";
    printarr(arr, n); //вывод массива на экран
    cout << "Выберите действие-" << endl
    << "1- добавить элемент в конец массива"<< endl
    << "2- добавить жлемент в начало массива"<< endl
    << "3- добавить элемент на k-ую позицию в массиве"<< endl
    << "4- удалить последний элемент массива"<< endl
    << "5- удалить первый элемент массива"<< endl
    << "6- удалить k-ый элемент массива"<< endl
    << "7- добавить элемент после первого встреченного заданного элемента"<< endl
    << "8- удаление всех элементов с заданным значением"<< endl
    << "0- выход из программы"<< endl; cin >> m;
    switch (m)
    {
      case 1:
      {
        number_n(number);
        reassign(arr, n, n + 1, number); //передаём n+1 тк добавление в конец
        break;
      }
      case 2:
      {
        number_n(number);
        reassign(arr, n, 1, number); //передаём n т.к добавление в начало
        break;
      }
      case 3:
      {
        unsigned int k;
        cout << "Введите позицию элемента: "; cin >> k;
        number_n(number);
        reassign(arr, n, k, number); //передаём k тк добавление на k-ую позицию
        break;
      }
      case 4:
      {
        del(arr, n, n);
        break;
      }
      case 5:
      {
        del(arr, n, 1);
        break;
      }
      case 6:
      {
        unsigned int k;
        cout << "Введите положение элемента: "; cin >> k;
        del(arr, n, k);
        break;
      }
      case 7:
      {
        double x;
        cout << "Введите элемент, после которого вы хотите добавить элемент: "; cin >> x;
        number_n(number);
        n7(arr, n, number, x);
        system("Pause");
        break;
      }
      case 8:
      {
        double x;
        cout << "Введите значения которые хотите удалить из массива: "; cin >> x;
        n8(arr, n, x);
        system("Pause");
        break;
      }
    }
  }
  delete[] arr;
}
void number_n(double &number) //функция выбора вставляемого числа
{
  int m = 1;
  double a = -100;
  double b = 100;
  cout << "Какое число хотите добавить?" << endl;
  cout << "1 -- рандомное " << endl;
  cout << "2 -- собстенное " << endl;
  cin >> m;
  switch (m)
  {
    case 1:
    {
      srand(time(0)); //рандомайзер веществнных чисел от -100 до 100
      number = a + (b - a) * (double) rand() /  RAND_MAX;
      m = 0;
      break;
    }
    case 2:
    {
      cout << "Введите свой номер: ";
      cin >> number;
      m = 0;
      break;
    }
    default:
    {
      cout << "Ошибка, попробуйте еще раз." << endl;
      break;
    }
  }
}
void fillarr(double* arr, int n)
{
  fstream A("data_array.txt", ios::in);
  for (int i = 0; i < n; i++)
  {
    A >> arr[i];
  }
  A.close();
}
void printarr(double* arr, int n)
{
    for (int i = 0; i < n; i++)
        cout << arr[i] << " ";
    cout << endl;
}
double* createarr(int n)
{
    double* arr = new double[n];
    return arr;
}
void reassign(double*& arr, int &n, int k, double number)
{
    k--;
    if (k >= 0 && k <= n)
    {
        double *buff = new double[n + 1]; //создаем новый массив на 1 больше изначального
        for (int i = 0; i < k; i++)  //заполняем новый массив до k-ого элемента
            buff[i] = arr[i];
        buff[k] = number; //вставляем рандомное или выбранное число на k-ую позицию
            n++;
        for (int i = k + 1; i < n; i++) //заполняем часть массива после k-ого элемента 
            buff[i] = arr[i - 1];
        delete[] arr; //удаляем изначальный массив
        arr = buff; //буферный массив делаем основным
    }
    else cout << "ошибка";
}
void del(double*& arr, int &n, int k)
{
    k--;
    if (k >= 0 && k <= n)
    {
        double* buff = new double[n - 1]; //создаём буферный массив на 1 меньшее исходного
        for (int i = 0; i < k; i++) //заполняем буферный массив элемнтами до k
            buff[i] = arr[i];
        n--;
        for (int i = k; i < n; i++) //заполняем массив после элемнта k включительно
            buff[i] = arr[i + 1];
        delete[] arr;
        arr = buff;
    }
    else cout << "ошибка";
}
void n7(double *&arr, int &n, double number, double x)
{
    int k = 0;
    for (int i = 0; i < n; i++) //находим элемент и запоминаем номер следующей за ним ячейки
    {
      if (arr[i] == x)
      {
        k = i + 1;
        break;
      }
    }
    if (k > 0)
    {
      double* buff = new double[n + 1]; //проводим процедуру добавления на k-ю позицию
      for (int i = 0; i < k; i++)
      {
        buff[i] = arr[i];
      }
      n++;
      buff[k] = number;
      for (int i = k + 1; i < n; i++)
      {
        buff[i] = arr[i - 1];
      }
      delete[] arr;
      arr = buff;
    }
    else cout << "элемент не найден" << endl;
}
void n8(double *&arr, int &n, double x)
{
  int k = 0;
  for (int i = 0; i < n; i++)
  {
    if (arr[i] == x)
    {
      k++;
    }
  }
  if (k > 0)
  {
    k = 0;
    double* buff = new double[n - k];
    for (int i = 0; i < n; i++)
    {
      if (arr[i] == x)
      {
        k++;
      }
      else buff[i - k] = arr[i];
    }
    n -= k;
    delete[] arr;
    arr = buff;
  }
  else cout << "элемент не найден" << endl;
}
int check_file()
{
  double a;
  int k = 0;
  fstream A("data_array.txt", ios::in);
  while (A >> a) //узнаём кл-во элементов массива в файле
  {
    k++;
  }
  A.close();
  return k;
}